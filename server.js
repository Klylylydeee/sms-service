const express = require("express");
const api = require('./api');
const axios = require('axios');
const FormData = require('form-data');

require("dotenv").config();

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const smsMessage = new api.SmsMessage();

app.listen(process.env.PORT || 5000, () =>
    console.log(`Running at port http://localhost:${process.env.PORT || 5000}/`)
);
 
app.get("/sendclick", (req, res) => {
    var smsApi = new api.SMSApi(process.env.clickSendUsername, process.env.clickSendPassword);
    var smsCollection = new api.SmsMessageCollection();
    smsMessage.from = "myNumber";
    smsMessage.to = "+639476303740";
    smsMessage.body = "test message";
    smsCollection.messages = [smsMessage];
    smsApi.smsSendPost(smsCollection).then(function(response) {
        res.send(response.body)
    }).catch(function(err){
        console.error(err.body);
    });
});
 
app.get("/itextmo", async (req, res) => { 
    try {
        let form = new FormData();
        form.append('1', '09476303740');
        form.append('2', 'klyde value');
        form.append('3', process.env.iTextMoUsername);
        form.append('passwd', process.env.iTextMoPassword);
        const headers = {
            ...form.getHeaders(),
            "Content-Length": form.getLengthSync()
        };
        let smsResponse = await axios.post('https://www.itexmo.com/php_api/api.php', form,  {headers});
        res.status(200).send({
            sucess: smsResponse.data
        })
    } catch (err) {
        console.log(err)
    }
});
